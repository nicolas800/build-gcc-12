# gcc download build and install script from sources

## Description

This is a gcc g++ download build and install script from sources tested for Linux gcc-12.

It requires sudo access.

## Usage

```
    ./install_gcc.sh
```

in your profile set: 

```
    export CC=/usr/local/gcc-12/bin/gcc
    export CXX=/usr/local/gcc-12/bin/g++
    export LD_LIBRARY_PATH=/usr/local/gcc-12/lib64/
```