#!/bin/sh
#
#   download build and install gcc 12
#
#   NOTE: use GCC_VERSION and/or PREFIX to further configure


export GCC_VERSION=12

sudo echo "installing gcc version ${GCC_VERSION}"
export PREFIX="/usr/local/gcc-${GCC_VERSION}"
export BRANCH_VER="releases/gcc-${GCC_VERSION}"

sudo apt install flex g++ --yes
git clone --depth 1  https://github.com/gcc-mirror/gcc.git --branch ${BRANCH_VER} --single-branch
cd gcc
./contrib/download_prerequisites
mkdir -p ../build-gcc-${GCC_VERSION} && cd ../build-gcc-${GCC_VERSION}
../gcc/configure -v --prefix=${PREFIX} --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu --enable-checking=release --enable-languages=c,c++ --disable-multilib
make -j8
sudo make install
